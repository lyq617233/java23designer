package com.surpass.designer.No7_适配器模式.adaptermode.turkey;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/10 21:58
 */
public interface Turkey {
    public void gobble();// 火鸡叫
    public void fly();
}
