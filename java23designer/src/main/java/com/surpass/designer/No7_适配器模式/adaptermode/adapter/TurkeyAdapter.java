package com.surpass.designer.No7_适配器模式.adaptermode.adapter;

import com.surpass.designer.No7_适配器模式.adaptermode.duck.Duck;
import com.surpass.designer.No7_适配器模式.adaptermode.turkey.Turkey;

/**
 * 火鸡适配器,使得火鸡能够出现鸭子的行为
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/10 22:00
 */
public class TurkeyAdapter implements Duck{

    private Turkey turkey;

    public TurkeyAdapter(Turkey turkey){
        this.turkey = turkey;
    }

    //火鸡调用鸭子叫声的方法,但是内部实现还是火鸡自身的叫声,只不过外部看起来像
    @Override
    public void quack() {
        turkey.gobble();
    }

    @Override
    public void fly() {
        for(int i = 0;i<6;i++) {
            turkey.fly();
        }
    }
}
