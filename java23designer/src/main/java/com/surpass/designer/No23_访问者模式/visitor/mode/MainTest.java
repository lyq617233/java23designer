package com.surpass.designer.No23_访问者模式.visitor.mode;

/**
 * 访问者模式,在不改动代码的情况下,增加新功能,极大提高维护性,扩展性
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:33
 */
public class MainTest {
    public static void main(String[] args) {
        Employees mEmployees = new Employees();

        mEmployees.Attach(new Employee("Tom", 4500, 8, 1));
        mEmployees.Attach(new Employee("Jerry", 6500, 10, 2));
        mEmployees.Attach(new Employee("Jack", 9600, 12, 3));

        //增加新功能,年终奖不必再改动代码
        mEmployees.Accept(new CompensationVisitor());
    }
}
