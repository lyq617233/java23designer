package com.surpass.designer.No23_访问者模式.visitor.mode;

/**
 * 年终奖访问者,专注于进行年终奖功能
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:27
 */
public class CompensationVisitor implements Visitor{
    @Override
    public void Visit(Element element) {
        Employee employee = (Employee) element;
        System.out.println(employee.getName() + "'s Compensation is "
                + (employee.getDegree() * employee.getVacationDays() * 10000));

    }
}
