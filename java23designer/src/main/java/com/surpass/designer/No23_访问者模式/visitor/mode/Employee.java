package com.surpass.designer.No23_访问者模式.visitor.mode;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:22
 */
public class Employee extends Element {
    private String name;
    private float income; //收入
    private int vacationDays; //年假
    private int degree; //级别

    public Employee(String name, float income, int vacationDays, int degree) {
        this.name = name;
        this.income = income;
        this.vacationDays = vacationDays;
        this.degree = degree;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getIncome() {
        return income;
    }

    public void setIncome(float income) {
        this.income = income;
    }

    public int getVacationDays() {
        return vacationDays;
    }

    public void setVacationDays(int vacationDays) {
        this.vacationDays = vacationDays;
    }

    public int getDegree() {
        return degree;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    //参数:访问者指派给该类
    @Override
    public void Accept(Visitor visitor) {
        //该类将自己反馈给访问者,双重指派
        visitor.Visit(this);
    }
}
