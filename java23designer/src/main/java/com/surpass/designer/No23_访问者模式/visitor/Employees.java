package com.surpass.designer.No23_访问者模式.visitor;

import java.util.HashMap;

/**
 * 员工管理
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:03
 */
public class Employees {
    private HashMap<String, Employee> employees;

    public Employees() {
        employees = new HashMap<>();
    }

    //入职
    public void Attach(Employee employee) {
        employees.put(employee.getName(), employee);
    }

    //离职
    public void Detach(Employee employee) {
        employees.remove(employee);
    }

    public Employee getEmployee(String name) {
        return employees.get(name);
    }

    //年终奖
    public void getCompensation() {
        for (Employee employee : employees.values()) {
            System.out.println(employee.getName() + "'s Compensation is "
            + (employee.getDegree() * employee.getVacationDays() * 100));
        }
    }
}
