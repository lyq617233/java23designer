package com.surpass.designer.No23_访问者模式.visitor.mode;

/**
 * 访问者
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:26
 */
public interface Visitor {
    void Visit(Element element);
}
