package com.surpass.designer.No23_访问者模式.visitor.mode;

import java.util.HashMap;

/**
 * 访问者模式的管理者
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:31
 */
public class Employees {
    private HashMap<String, Employee> employees;

    public Employees() {
        employees = new HashMap<>();
    }

    //入职
    public void Attach(Employee employee) {
        employees.put(employee.getName(), employee);
    }

    //离职
    public void Detach(Employee employee) {
        employees.remove(employee);
    }

    public Employee getEmployee(String name) {
        return employees.get(name);
    }

    //访问者注入
    public void Accept(Visitor visitor) {
        for (Employee e : employees.values()) {
            e.Accept(visitor);
        }
    }

}
