package com.surpass.designer.No23_访问者模式.visitor;

/**
 *
 * 传统模式设计:新功能放入管理类Employees
 * 存在的问题:每次添加新功能时,都要进行重新编译,继承,违背开闭原则:对功能扩展开放,对修改关闭
 * 那么功能加入Employee类呢??其实也是存在一样的问题
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 23:07
 */
public class MainTest {
    public static void main(String[] args) {
        Employees mEmployees = new Employees();

        mEmployees.Attach(new Employee("Tom", 4500, 8, 1));
        mEmployees.Attach(new Employee("Jerry", 6500, 10, 2));
        mEmployees.Attach(new Employee("Jack", 9600, 12, 3));
        mEmployees.getCompensation();
    }
}
