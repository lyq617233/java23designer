package com.surpass.designer.No1_策略模式.stimulateduck.oo;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:08
 */
public class StimulatDuck {
    public static void main(String[] args) {
        GreenHeadDuck mGreenHeadDuck = new GreenHeadDuck();
        RedHeadDuck mredHeadDuck = new RedHeadDuck();

        mGreenHeadDuck.display();

        mGreenHeadDuck.Quack();
        mGreenHeadDuck.swim();

        mredHeadDuck.display();

        mredHeadDuck.Quack();
        mredHeadDuck.swim();

        mGreenHeadDuck.Fly();
        mredHeadDuck.Fly();
    }
}
