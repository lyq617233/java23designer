package com.surpass.designer.No1_策略模式.stimulateduck.duck;

import com.surpass.designer.No1_策略模式.stimulateduck.flybehavior.FlyBehavior;
import com.surpass.designer.No1_策略模式.stimulateduck.quackbehavior.QuackBehavior;

/**
 * 接口加实现的方式,行为组合更加方便
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:26
 */
public abstract class Duck {
    FlyBehavior mFlyBehavior;
    QuackBehavior mQuackBehavior;

    public Duck(){}

    public void Fly() {
        mFlyBehavior.fly();
    }

    public void Quack() {
        mQuackBehavior.quack();
    }

    public void swim(){
        System.out.println("--im swim--");
    }

    public abstract void display();

    public void SetFlyBehavior(FlyBehavior fb) {
        mFlyBehavior = fb;
    }

    public void SetQuackBehavior(QuackBehavior qb) {
         mQuackBehavior = qb;
    }
}
