package com.surpass.designer.No1_策略模式.stimulateduck.oo;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 17:06
 */
public class GreenHeadDuck extends Duck{

    @Override
    public void display() {
        System.out.println("**GreenHead**");
    }

    //覆盖
    @Override
    public void Fly() {
        System.out.println("--no fly--");
    }
}
