package com.surpass.designer.No22_原型模式.protomode;

import java.util.Random;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 21:53
 */
public class MainTest {
    public static void main(String[] args) {
        int i = 0;
        int MAX_COUNT = 10;
        EventTemplate et = new EventTemplate("9月份信用卡账单", "国庆抽奖活动...");

        Mail mail = new Mail(et);

        while (i < MAX_COUNT) {
            //以下是每封邮件不同的地方

            mail.setContent(getRandString(5) + ",先生(女士): 你的信用卡账单..." +
                    mail.getTail());
            mail.setReceiver(getRandString(5) + "@" + getRandString(8) + ".com");
            //发送邮件
            sendMail(mail);
            i++;
        }



    }

    //随机产生用户
    public static String getRandString( int maxLength) {
        String source = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        StringBuffer sb = new StringBuffer();
        Random rand = new Random();
        for (int i = 0; i < maxLength; i++) {
            sb.append(source.charAt(rand.nextInt(source.length())));
        }
        return sb.toString();
    }

    public static void sendMail(Mail mail) {
        System.out.println("标题:" + mail.getSubject() + "\t收件人:" +
                mail.getReceiver() + "\t内容:" + mail.getContent() + "\t....发送成功!");
    }
}
