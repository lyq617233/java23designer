package com.surpass.designer.No22_原型模式.protomode.proto;

import com.surpass.designer.No22_原型模式.protomode.EventTemplate;

import java.util.ArrayList;

/**
 * Cloneable,java自带克隆类,原型模式:通过内存复制,克隆出一个新对象,对新对象进行操作,
 * 比new新对象开辟内存空间节省大量时间
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 22:16
 */
public class Mail implements Cloneable{
    private String receiver;
    private String subject;
    private String content;
    private String tail;
    private ArrayList<String> ars; //不会拷贝
    public Mail(EventTemplate et) {
        this.tail = et.getEventContent();
        this.subject = et.getEventSubject();
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    //直接复制对象
    @Override
    public Mail clone() {
        Mail mail = null;
        try {
            mail = (Mail) super.clone();
            mail.ars = (ArrayList<String>) this.ars.clone(); //ars手动拷贝给新对象
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        return mail;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTail() {
        return tail;
    }

    public void setTail(String tail) {
        this.tail = tail;
    }
}
