package com.surpass.designer.No22_原型模式.protomode;

/**
 * 邮件类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/9 21:50
 */
public class Mail {
    private String receiver;
    private String subject;
    private String content;
    private String tail;

    public Mail(EventTemplate et) {
        this.tail = et.getEventContent();
        this.subject = et.getEventSubject();
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getSubject() {
        return subject;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getTail() {
        return tail;
    }

    public void setTail(String tail) {
        this.tail = tail;
    }
}
