package com.surpass.designer.No16_生成器模式.builderms;

import com.surpass.designer.No16_生成器模式.builderms.builder.AbsBuilder;

/**
 * 指导类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 22:44
 */
public class Director {
    private AbsBuilder builder;

    public Director(AbsBuilder builder) {
        this.builder = builder;
    }

    public void setBuilder(AbsBuilder builder) {
        this.builder = builder;
    }

    public void construct() {
        builder.buildvacation();
        builder.getVacation().showInfo();
    }
}
