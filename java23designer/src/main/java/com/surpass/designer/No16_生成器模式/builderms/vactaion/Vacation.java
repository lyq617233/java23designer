package com.surpass.designer.No16_生成器模式.builderms.vactaion;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Vacation度假计划
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 22:16
 */
public class Vacation {
    private ArrayList<VacationDay> mVacationDayLst;
    private Date mStDate;
    private int mDays = 0;
    private VacationDay mVacationDay;

    public Vacation(String std) {
        mVacationDayLst = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mStDate = sdf.parse(std);
            mVacationDay = new VacationDay(mStDate);
            mVacationDayLst.add(mVacationDay);
            mDays++;
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void setStDate(String std) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        try {
            mStDate = sdf.parse(std);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public Date getStDate() {
        return mStDate;
    }

    //加一天
    public void addDay() {
        mVacationDay = new VacationDay(nextDate(mDays));
        mVacationDayLst.add(mVacationDay);
        mDays++;
    }

    public boolean setVacationDay(int i) {
        if ((i > 0) && (i < mVacationDayLst.size())) {
            mVacationDay = mVacationDayLst.get(i);
            return true;
        }
        mVacationDay = null;
        return false;
    }

    public void setHotel(String mHotels) {
        mVacationDay.setHotel(mHotels);
    }

    public void addTicket(String ticket) {
        mVacationDay.addTicket(ticket);
    }

    public void addEvent(String event) {
        mVacationDay.addEvent(event);
    }

    public void showInfo() {
        for (int i = 0; i < mVacationDayLst.size(); i++) {
            System.out.println("**" + (i + 1) + "day**");
            System.out.println(mVacationDayLst.get(i).showInfo());
        }
    }

    private Date nextDate(int n) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(mStDate);
        cal.add(Calendar.DATE, n);
        return cal.getTime();
    }

}
