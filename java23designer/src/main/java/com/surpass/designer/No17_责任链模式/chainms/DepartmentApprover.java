package com.surpass.designer.No17_责任链模式.chainms;

/**
 * 部长
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:08
 */
public class DepartmentApprover extends Approver {


    public DepartmentApprover(String Name) {
        super(Name + " DepartmentApprover");
    }

    @Override
    public void ProcessRequest(PurchaseRequest request) {
        if ((5000 <= request.GetSum()) && (request.GetSum() < 10000)) {
            System.out.println("**This request " + request.GetID()
                    + " will be handled by " +
                    this.Name);
        } else {
            successor.ProcessRequest(request);
        }
    }
}
