package com.surpass.designer.No17_责任链模式.chainms;

/**
 * 副总
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:10
 */
public class VicePresidentApprover extends Approver{


    public VicePresidentApprover(String Name) {
        super(Name + " VicePresidentApprover");
    }

    @Override
    public void ProcessRequest(PurchaseRequest request) {
        if ((10000 <= request.GetSum()) && (request.GetSum() < 50000)) {
            System.out.println("**This request " + request.GetID()
                    + " will be handled by " +
                    this.Name);
        } else {
            successor.ProcessRequest(request);
        }
    }
}
