package com.surpass.designer.No3_装饰者模式.coffeebar.coffee;

/**
 * 咖啡的一种,特有属性
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:15
 */
public class Decaf extends Coffee {
    public Decaf() {
        super.setDescription("Decaf");
        super.setPrice(3.0f);
    }
}
