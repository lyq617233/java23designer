package com.surpass.designer.No3_装饰者模式.coffeebar.decorator;

import com.surpass.designer.No3_装饰者模式.coffeebar.Drink;

/**
 * 牛奶调料,单品(装饰)
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:25
 */
public class Milk extends Decorator{
    public Milk(Drink Obj){
        super(Obj);

        super.setDescription("Milk");
        super.setPrice(2.0f);

    }
}
