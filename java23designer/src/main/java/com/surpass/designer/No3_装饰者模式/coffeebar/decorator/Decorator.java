package com.surpass.designer.No3_装饰者模式.coffeebar.decorator;

import com.surpass.designer.No3_装饰者模式.coffeebar.Drink;

/**
 * 装饰者,可以是单品,也可以是被包装的单品,所以采用Decorator超类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:22
 */
public class Decorator extends Drink{

    private Drink Obj;

    public Decorator(Drink Obj){
        this.Obj = Obj;
    }

    @Override
    public float cost() {
        //递归调用
        return super.getPrice() + Obj.cost();
    }

    @Override
    public String getDescription() {
        return super.description + "-" + super.getPrice() +
                "&&" + Obj.getDescription();
    }
}
