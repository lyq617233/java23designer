package com.surpass.designer.No3_装饰者模式.coffeebar.coffee;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 22:20
 */
public class ShortBlack extends Coffee {
    public ShortBlack(){
        super.setDescription("ShortBlack");
        super.setPrice(1.0f);
    }
}
