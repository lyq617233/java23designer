package com.surpass.designer.No12_状态模式.statemode;

/**
 * 传统设计方案:公司增加新需求,加入游戏元素:有10%的概率可以拿到2粒糖果
 * 1.增加一个新状态赢家状态,每一个动作的所有switch均要修改,修改开放,耦合大,维护难
 * 2.基于接口开发,不应基于功能开发
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/29 0:07
 */
public class MainTest {
    public static void main(String[] args) {
        //初始化糖果机
        CandyMachine mCandyMachine = new CandyMachine(1);

        //状态
        mCandyMachine.printstate();

        //放入硬币
        mCandyMachine.insertCoin();
        //状态
        mCandyMachine.printstate();

        //转动把手
        mCandyMachine.turnCrank();

        //状态
        mCandyMachine.printstate();

        //再操作一波
        mCandyMachine.insertCoin();
        mCandyMachine.printstate();

        mCandyMachine.turnCrank();

        mCandyMachine.printstate();
    }
}
