package com.surpass.designer.No20_中介者模式.mediator;

/**
 * 中介者,封装对象之间关系
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 20:40
 */
public interface Mediator {
    public abstract void Register(String colleagueName, Colleague colleague);

    public abstract void GetMessage(int stateChange, String colleagueName);

    public abstract void SendMessage();
}
