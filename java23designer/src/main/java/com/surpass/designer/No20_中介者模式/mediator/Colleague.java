package com.surpass.designer.No20_中介者模式.mediator;

/**
 * 同事
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 21:25
 */
public abstract class Colleague {
    private Mediator mediator;
    public String name;

    public Colleague(Mediator mediator, String name) {
        this.mediator = mediator;
        this.name = name;
    }

    public Mediator GetMediator() {
        return this.mediator;
    }

    //给中介送消息
    public abstract void SendMessage(int stateChange);

}
