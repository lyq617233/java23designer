package com.surpass.designer.No20_中介者模式.mediator;

/**
 * 同事闹铃
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 21:27
 */
public class Alarm extends Colleague {

    public Alarm(Mediator mediator, String name) {
        super(mediator, name);
        mediator.Register(name, this);
    }

    @Override
    public void SendMessage(int stateChange) {
        this.GetMediator().GetMessage(stateChange, this.name);
    }

    public void SendAlarm(int stateChange) {
        SendMessage(stateChange);
    }
}
