package com.surpass.designer.No15_桥接模式.bridgems.control;

/**
 * 夏普厂家
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 21:15
 */
public class SharpControl implements Control{
    @Override
    public void On() {
        System.out.println("***Open Sharp TV***");
    }

    @Override
    public void Off() {
        System.out.println("***Off Sharp TV***");
    }

    @Override
    public void setChannel(int ch) {
        System.out.println("***The Sharp TV Channel is setted" + ch + "***");
    }

    @Override
    public void setVolume(int vol) {
        System.out.println("***The Sony TV Volume is setted  " + vol + "***");
    }
}
