package com.surpass.designer.No15_桥接模式.bridgems;

/**
 * 极简方案主类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 0:03
 */
public class MainTest {
    public static void main(String[] args) {
        LGTvControl mLgTvControl = new LGTvControl();
        SonyTvControl mSonyTvControl = new SonyTvControl();

        mLgTvControl.Onoff();
        mLgTvControl.nextChannel();
        mLgTvControl.nextChannel();
        mLgTvControl.preChannel();
        mLgTvControl.Onoff();


        mSonyTvControl.Onoff();
        mSonyTvControl.preChannel();
        mSonyTvControl.preChannel();
        mSonyTvControl.preChannel();
        mSonyTvControl.Onoff();

    }
}
