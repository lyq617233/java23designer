package com.surpass.designer.No15_桥接模式.bridgems.control;

/**
 * 索尼遥控器接口实现类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/3 23:49
 */
public class SonyControl implements Control{

    @Override
    public void On() {
        System.out.println("*Open Sony TV*");
    }
    @Override
    public void Off() {
        System.out.println("*Off Sony TV*");
    }

    @Override
    public void setChannel(int ch) {
        System.out.println("*The Sony TV Channel is setted  " + ch + "*");
    }

    @Override
    public void setVolume(int vol) {
        System.out.println("*The Sony TV Volume is setted  " + vol + "*");
    }
}
