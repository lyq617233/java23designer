package com.surpass.designer.No15_桥接模式.bridgems.bridge;

import com.surpass.designer.No15_桥接模式.bridgems.control.Control;

/**
 * 新遥控器功能,数字换频道,回退上次频道
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/4 21:37
 */
public class NewTvControl extends TvControlabs {

    private int ch = 0;
    private boolean ison = false;
    private int prech = 0;

    public NewTvControl(Control mControl) {
        super(mControl);
    }

    @Override
    public void Onoff() {
        if (ison) {
            ison = false;
            mControl.Off();
        } else {
            ison = true;
            mControl.On();
        }
    }

    @Override
    public void nextChannel() {
        prech = ch;
        ch++;
        mControl.setChannel(ch);
    }

    @Override
    public void preChannel() {
        prech = ch;
        ch--;
        if (ch < 0) {
            ch = 200;
        }
        mControl.setChannel(ch);
    }

    //新增加功能
    public void Back() {
        mControl.setChannel(prech);
    }

    public void setChannel(int nch) {
        prech = ch;
        ch = nch;
        mControl.setChannel(ch);
    }


}
