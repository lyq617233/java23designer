package com.surpass.designer.No15_桥接模式.bridgems.control;

/**
 * LG遥控器接口实现类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/3 23:49
 */
public class LGControl implements Control{

    @Override
    public void On() {
        System.out.println("*Open LG TV*");
    }
    @Override
    public void Off() {
        System.out.println("*Off LG TV*");
    }

    @Override
    public void setChannel(int ch) {
        System.out.println("*The LG TV Channel is setted  " + ch + "*");
    }

    @Override
    public void setVolume(int vol) {
        System.out.println("*The LG TV Volume is setted  " + vol + "*");
    }
}
