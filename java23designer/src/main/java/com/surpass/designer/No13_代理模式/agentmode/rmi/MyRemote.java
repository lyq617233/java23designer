package com.surpass.designer.No13_代理模式.agentmode.rmi;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * 接口里面放要被调用的方法
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 20:12
 */
public interface MyRemote extends Remote {
    public String sayHello() throws RemoteException;
}
