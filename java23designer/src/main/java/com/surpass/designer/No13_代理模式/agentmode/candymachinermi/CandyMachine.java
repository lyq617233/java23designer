package com.surpass.designer.No13_代理模式.agentmode.candymachinermi;


import java.io.Serializable;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/1 23:10
 */
public class CandyMachine extends UnicastRemoteObject implements CandyMachineRemote {
    State mSoldOutState;
    State mOnReadyState;
    State mHasCoin;
    State mSoldState;
    State mWinnerState;
    private String location = ""; //位置
    private State state;
    private int count = 0; //糖果个数

    public String getLocation() {
        return location;
    }

    public CandyMachine(String location, int count) throws RemoteException {
        this.location = location;
        this.count = count;
        mSoldOutState = new SoldOutState(this);
        mOnReadyState = new OnReadyState(this);
        mHasCoin = new HasCoin(this);
        mSoldState = new SoldState(this);
        mWinnerState = new WinnerState(this);
        if (count > 0) {
            state = mOnReadyState; //待机状态
        } else {
            state = mSoldOutState; //售罄状态
        }
    }

    public void insertCoin() {
        state.insertCoin();
    }

    public void returnCoin() {
        state.returnCoin();
    }

    //封装转动把手后的结果,可能有糖果掉出,可能提示没有硬币
    public void turnCrank() {
        //这一步可能修改了状态
        state.trunCrank();
        state.dispense();
        //两个state状态有可能不一样,返回不一样的结果
    }

    public void setState(State state) {
        this.state = state;
    }

    public int getCount() {
        return count;
    }

    void releaseCandy() {
        if (count > 0) {
            count = count - 1;
            System.out.println("一个糖果正在掉出");
        }
    }

    public void printstate() {
        state.printstate();
    }

    public State getState() {
        return state;
    }
}
