package com.surpass.designer.No13_代理模式.agentmode.dyn;

/**
 * 信息接口
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 23:45
 */
public interface PersonBean {
    String getName();
    String getGender();
    String getInterests();
    int getHotOrNotRating();//打分

    void setName(String name);
    void setGender(String gender);
    void setInterests(String interests);
    void setHotOrNotRating(int rating);

}
