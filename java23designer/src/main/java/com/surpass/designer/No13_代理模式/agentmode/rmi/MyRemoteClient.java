package com.surpass.designer.No13_代理模式.agentmode.rmi;

import java.rmi.Naming;
import java.rmi.Remote;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 20:19
 */
public class MyRemoteClient {
    public static void main(String[] args) {
        new MyRemoteClient().go();
    }

    public void go() {
        try {
            MyRemote service = (MyRemote) Naming.lookup("rmi://localhost:6600/RemoteHello");
            String s = service.sayHello();
            System.out.println(s);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
