package com.surpass.designer.No13_代理模式.agentmode.dyn;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * 自己给自己设置姓名性别爱好,但不可给自己打分
 * 通过InvocationHandler类实现以上逻辑
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 23:54
 */
public class OwnerInvocationHandler implements InvocationHandler {
    PersonBean person; //控制访问对象

    public OwnerInvocationHandler(PersonBean person) {
        this.person = person;
    }

    /**
      * @author cmqzyd0700@163.com
      * @since  2019/7/2 23:56
      * @param proxy    代理
      * @param method   方法
      * @param args     参数
      * @return java.lang.Object
      */
    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        if (method.getName().startsWith("get")) {
            return method.invoke(person, args);
        } else if (method.getName().equals("setHotOrNotRating")) {
            //不允许打分
            return new IllegalAccessException();
        } else if (method.getName().startsWith("set")) {
            return method.invoke(person, args);
        }
        return null;
    }
}
