package com.surpass.designer.No13_代理模式.agentmode.candymachinermi;

import java.rmi.Naming;
import java.rmi.registry.LocateRegistry;

/**
 * 服务端
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/2 22:54
 */
public class RemoteMainTest {
    public static void main(String[] args) {
        try {
            CandyMachine service = new CandyMachine("test1", 7);
            LocateRegistry.createRegistry(6602);
            Naming.rebind("rmi://localhost:6602/test1",service);
            service.insertCoin();
            service = new CandyMachine("test2", 5);
            Naming.rebind("rmi://localhost:6602/test2",service);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }
}
