package com.surpass.designer.No9_模板模式.hotdrink;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:27
 */
public class Tea extends HotDrink{

    @Override
    public void brew() {
        System.out.println("Brewing Tea");
    }

    @Override
    public void addCondiments() {
        System.out.println("Adding Lemon");
    }
}
