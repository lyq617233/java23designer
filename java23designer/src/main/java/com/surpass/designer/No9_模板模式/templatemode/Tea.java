package com.surpass.designer.No9_模板模式.templatemode;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:11
 */
public class Tea {

    public void prepareRecipe() {
        boilWater();
        brewCoffee();
        pourInCup();
        addLemon();
    }

    public void boilWater() {
        System.out.println("Boiling water");
    }

    public void brewCoffee() {
        System.out.println("Brewing Tea");
    }

    public void pourInCup() {
        System.out.println("Pouring into cup");
    }

    public void addLemon() {
        System.out.println("Adding Lemon");
    }
}
