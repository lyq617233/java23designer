package com.surpass.designer.No9_模板模式.templatemode.duck;

import java.util.Arrays;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 17:59
 */
public class MainTest {
    public static void main(String[] args) {
        Duck[] ducks = {
                new Duck("Duck1", (int) (Math.random() * 100)),
                new Duck("Duck2", (int) (Math.random() * 100)),
                new Duck("Duck3", (int) (Math.random() * 100)),
                new Duck("Duck4", (int) (Math.random() * 100)),
                new Duck("Duck5", (int) (Math.random() * 100)),
                new Duck("Duck6", (int) (Math.random() * 100)),
                new Duck("Duck7", (int) (Math.random() * 100)),
                new Duck("Duck8", (int) (Math.random() * 100)),
                new Duck("Duck9", (int) (Math.random() * 100)),
        };
        System.out.println("before sort:");
        display(ducks);
        Arrays.sort(ducks);
        System.out.println("after sort:");
        display(ducks);

    }

    private static void display(Duck[] ducks) {
        for(int i = 0;i<ducks.length;i++) {
            System.out.println(ducks[i].toString());
        }
    }
}
