package com.surpass.designer.No10_迭代器模式.iteratormode.iterator;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 21:26
 */
public class MainTest {
    public static void main(String[] args) {
        Waitress mWaitress = new Waitress();
        CakeHouseMenu mCakeHouseMenu = new CakeHouseMenu();
        DinerMenu mDinerMenu = new DinerMenu();

        //蛋糕店迭代器
        mWaitress.addIterator(mCakeHouseMenu.getIterator());
        //中餐厅迭代器
        mWaitress.addIterator(mDinerMenu.getIterator());
        mWaitress.printMenu();
    }


}
