package com.surpass.designer.No10_迭代器模式.iteratormode;

import java.util.ArrayList;

/**
 * 蛋糕店使用ArrayList管理菜单
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/25 23:17
 */
public class CakeHouseMenu {
    private ArrayList<MenuItem> menuItems;

    public CakeHouseMenu() {
        menuItems = new ArrayList<>();

        addItem("KFC Cake Breakfast", "boiled eggs&toast&cabbage", true, 3.99f);
        addItem("MDL Cake Breakfast", "fried eggs&toast", false, 3.59f);
        addItem("Stawberry Cake", "fresh stawberry", true, 3.29f);
        addItem("Regular Cake Breakfast", "toast&sausage", true, 2.59f);
    }

    private void addItem(String name, String description, boolean vegetable,
                         float price) {
        MenuItem menuItem = new MenuItem(name, description, vegetable, price);
        menuItems.add(menuItem);
    }

    public ArrayList<MenuItem> getMenuItems() {
        return menuItems;
    }
}
