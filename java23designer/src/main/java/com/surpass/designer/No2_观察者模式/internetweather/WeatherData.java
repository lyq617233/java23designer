package com.surpass.designer.No2_观察者模式.internetweather;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 18:34
 */
public class WeatherData {
    private float mTemperature; //温度
    private float mPressure; //气压
    private float mHumidity; //湿度
    //公告板
    private CurrentConditions mCurrentConditions;

    public WeatherData(CurrentConditions mCurrentConditions) {
        this.mCurrentConditions = mCurrentConditions;
    }

    public float getmTemperature() {
        return mTemperature;
    }

    public float getmPressure() {
        return mPressure;
    }

    public float getmHumidity() {
        return mHumidity;
    }

    public void dataChange(){
        mCurrentConditions.update(getmTemperature(),getmPressure(),getmHumidity());
    }

    //模拟气象站传输数据
    public void setData(float mTemperature, float mPressure, float mHumidity) {
        this.mTemperature = mTemperature;
        this.mPressure = mPressure;
        this.mHumidity = mHumidity;
        dataChange();
    }
}
