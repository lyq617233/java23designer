package com.surpass.designer.No2_观察者模式.internetweather.observer;

/**
 * 观察者
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/8 19:04
 */
public interface Observer {
    public void update(float mTemperature, float mPressure, float mHumidity);
}
