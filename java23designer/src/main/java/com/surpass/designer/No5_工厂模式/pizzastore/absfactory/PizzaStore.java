package com.surpass.designer.No5_工厂模式.pizzastore.absfactory;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 16:31
 */
public class PizzaStore {
    public static void main(String[] args) {
        OrderPizza mOrderPizza;
        mOrderPizza = new OrderPizza(new LDFactory());
    }
}
