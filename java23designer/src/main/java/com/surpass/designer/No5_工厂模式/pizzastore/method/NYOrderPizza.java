package com.surpass.designer.No5_工厂模式.pizzastore.method;

import com.surpass.designer.No5_工厂模式.pizzastore.pizza.NYCheesePizza;
import com.surpass.designer.No5_工厂模式.pizzastore.pizza.NYPepperPizza;
import com.surpass.designer.No5_工厂模式.pizzastore.pizza.Pizza;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 16:08
 */
public class NYOrderPizza extends OrderPizza{

    @Override
    Pizza createPizza(String ordertype) {
        Pizza pizza = null;
        if (ordertype.equals("cheese")) {
            pizza = new NYCheesePizza();
        } else if (ordertype.equals("pepper")) {
            pizza = new NYPepperPizza();
        }
        return pizza;
    }
}
