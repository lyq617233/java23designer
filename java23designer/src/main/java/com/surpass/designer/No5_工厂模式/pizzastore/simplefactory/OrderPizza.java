package com.surpass.designer.No5_工厂模式.pizzastore.simplefactory;

import com.surpass.designer.No5_工厂模式.pizzastore.pizza.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * 简单工厂设计方案,每增加一种pizza,不需要进行修改,唯一要修改的就是工厂的pizza获取和pizza族
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 15:25
 */
public class OrderPizza {

    SimplePizzaFactory mSimplePizzaFactory;

    public OrderPizza(SimplePizzaFactory mSimplePizzaFactory){
        setFactory(mSimplePizzaFactory);
    }

    public void setFactory(SimplePizzaFactory mSimplePizzaFactory){
        Pizza pizza = null;
        String ordertype;

        this.mSimplePizzaFactory = mSimplePizzaFactory;

        do{
            ordertype = gettype();
            pizza = mSimplePizzaFactory.CreatePizza(ordertype);
            if (pizza != null) {
                pizza.prepare();
                pizza.bake();
                pizza.cut();
                pizza.box();
            }
        }while (true);
    }

    private String gettype() {
        try {
            BufferedReader strin = new BufferedReader(new InputStreamReader(
                    System.in
            ));
            System.out.println("input pizza type:");
            String str = strin.readLine();
            return str;
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
    }
}
