package com.surpass.designer.No11_组合模式.composemode;

import java.util.Iterator;
import java.util.Stack;

/**
 * 组合迭代器高阶设计,递归遍历
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 23:54
 */
public class ComposeIterator implements Iterator{

    private Stack<Iterator> stack = new Stack<>();

    public ComposeIterator(Iterator iterator) {
        stack.push(iterator);
    }

    @Override
    public boolean hasNext() {
        if (stack.isEmpty()) {
            return false;
        }
        //栈最上层的迭代器,一点一点往下遍历
        Iterator iterator = stack.peek();
        if (!iterator.hasNext()) {
            stack.pop();
            return hasNext();
        } else {
            return true;
        }

    }

    @Override
    public Object next() {
        if (hasNext()) {
            Iterator iterator = stack.peek();
            MenuComponent mMenuComponent = (MenuComponent) iterator.next();
            stack.push(mMenuComponent.getIterator());
            return mMenuComponent;
        }
        return null;
    }

    @Override
    public void remove() {

    }
}
