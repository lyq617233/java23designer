package com.surpass.designer.No11_组合模式.composemode;

import java.util.Iterator;

/**
 * 空迭代器:处理方便而使用,不需要判断空
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/26 23:53
 */
public class NullIterator implements Iterator {
    @Override
    public boolean hasNext() {
        return false;
    }

    @Override
    public Object next() {
        return null;
    }

    @Override
    public void remove() {

    }
}
