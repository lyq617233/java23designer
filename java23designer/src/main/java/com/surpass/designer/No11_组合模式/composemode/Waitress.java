package com.surpass.designer.No11_组合模式.composemode;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/28 21:20
 */
public class Waitress {
    private ArrayList<MenuComponent> menuComponents = new ArrayList<>();

    public Waitress() {

    }

    public void addComponent(MenuComponent mMenuComponent) {
        menuComponents.add(mMenuComponent);
    }

    //核心
    public void printMenu() {
        Iterator iterator; //菜单项迭代器
        MenuComponent menuItem; // 菜单项
        for (int i = 0; i < menuComponents.size(); i++) {
            //菜单,或者子菜单
            menuComponents.get(i).print();
            //迭代器,打印菜单项或者子菜单项
            iterator = menuComponents.get(i).getIterator();
            //调用组合迭代器ComposeIterator方法
            while (iterator.hasNext()) {
                menuItem = (MenuComponent) iterator.next();
                menuItem.print();
            }
        }
    }

    //打印是否是素食类型的菜单
    public void printVegetableMenu() {
        Iterator iterator;
        MenuComponent menuItem;
        for (int i = 0; i < menuComponents.size(); i++) {
            menuComponents.get(i).print();
            iterator = menuComponents.get(i).getIterator();

            while (iterator.hasNext()) {
                menuItem = (MenuComponent) iterator.next();
                if (menuItem.isVegetable()) {
                    menuItem.print();
                }
            }
        }
    }
}
