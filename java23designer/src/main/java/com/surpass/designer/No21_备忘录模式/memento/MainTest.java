package com.surpass.designer.No21_备忘录模式.memento;

/**
 * 备忘录模式测试主类
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 22:33
 */
public class MainTest {
    public static void main(String[] args) {
        MementoCaretaker mMementoCaretaker = new MementoCaretaker();
        Originator mOriginator = new Originator();
        Originator2 mOriginator2 = new Originator2();

        System.out.println("*****Originator*****");
        mOriginator.testState1();
        mMementoCaretaker.saveMemento("Originator", mOriginator.createMemento());
        mOriginator.showState();
        mOriginator.testState2();
        mOriginator.showState();
        mOriginator.restoreMemento(mMementoCaretaker.retrieveMemento("Originator"));
        mOriginator.showState();

        System.out.println("*****Originator 2*****");
        mOriginator2.testState1();
        mOriginator2.showState();
        mMementoCaretaker.saveMemento("Originator2", mOriginator2.createMemento());
        mOriginator2.testState2();
        mOriginator2.showState();
        mOriginator2.restoreMemento(mMementoCaretaker.retrieveMemento("Originator2"));
        mOriginator2.showState();

        System.out.println("*****Originator&&Originator 2*****");

        //发起者1号调用发起者2号的备忘录,报错提醒
        //mOriginator.restoreMemento(mMementoCaretaker.retrieveMemento("Originator2"));
        //mOriginator.showState();

    }
}
