package com.surpass.designer.No21_备忘录模式.memento;

import java.util.HashMap;

/**
 * 发起者1号
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 22:16
 */
public class Originator {

    //状态信息
    private HashMap<String, String> state;

    public Originator() {
        state = new HashMap<>();
    }

    public MementoIF createMemento() {
        return new Memento(state);
    }

    public void restoreMemento(MementoIF memento) {
        state = ((Memento) memento).getState();
    }

    public void showState() {
        System.out.println("now state:" + state.toString());
    }

    //游戏存档1
    public void testState1() {
        state.put("blood", "500");
        state.put("progress", "gate1 end");
        state.put("enemy", "5");
    }

    //游戏存档2
    public void testState2() {
        state.put("blood", "450");
        state.put("progress", "gate3 start");
        state.put("enemy", "3");
    }


    //发起者1号的内部备忘录
    private class Memento implements MementoIF {
        private HashMap<String, String> state;

        private Memento(HashMap state) {
            this.state = new HashMap<>(state);


            //这个语句不能用,使用该语句会引起state内存指向同一区域
            //这样外部可以改动,无法保证安全
            //this.state = state;
        }

        private HashMap getState() {
            return state;
        }

        private void setState(HashMap state) {
            this.state = state;
        }
    }


}
