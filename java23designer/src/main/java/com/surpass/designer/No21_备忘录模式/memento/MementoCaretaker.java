package com.surpass.designer.No21_备忘录模式.memento;

import java.util.HashMap;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/8 22:14
 */
public class MementoCaretaker {
    private HashMap<String, MementoIF> mementomap;

    public MementoCaretaker() {
        mementomap = new HashMap<>();
    }

    public MementoIF retrieveMemento(String name) {
        return mementomap.get(name);
    }

    //备忘录赋值方法
    public void saveMemento(String name, MementoIF memento) {
        this.mementomap.put(name, memento);
    }
}
