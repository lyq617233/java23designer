package com.surpass.designer.No18_蝇量模式.flyweight;

/**
 * 景观项目,树对象
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/7/5 21:56
 */
public class Tree {
    private int xCoord,yCoord,age; //x坐标,y坐标,年龄

    public Tree(int xCoord, int yCoord, int age) {
        this.xCoord = xCoord;
        this.yCoord = yCoord;
        this.age = age;
    }

    public void display() {

    }
}
