package com.surpass.designer.No4_单例模式.singleton;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 14:38
 */
public class Test {
    public class Abc{
        private Abc(){}
    }

    public class Cbd{
        public Cbd(){
            Abc n1,n2;
            n1 = new Abc();
            n2 = new Abc();
        }
    }
}
