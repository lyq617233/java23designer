package com.surpass.designer.No4_单例模式.singleton;

/**
 * 原始巧克力工厂,目的:工厂1和2使用同一个锅炉
 * 采用单例模式,实现以上结果
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 14:44
 */
public class ChocolateFactory2 {
    private boolean empty; //空的
    private boolean boiled;//是否加热

    //优化三:volatile:给编译器用,处理多线程安全,同时getInstance方法增加双重检查加锁
    public volatile static ChocolateFactory2 uniqueInstance = null;
    //第二种方案,急切创建实例:可能存在资源浪费
    //public static ChocolateFactory2 uniqueInstance = new ChocolateFactory2();

    private ChocolateFactory2(){
        empty = true;
        boiled = false;
    }

    //优化一:第一种方案,可能存在资源占用过多
    //如果不加synchronized:方法存在多线程问题:第一个线程new对象之前切换到第二个线程
    //造成new两个对象
//    public static synchronized ChocolateFactory2 getInstance(){
//        if (uniqueInstance == null) {
//            uniqueInstance = new ChocolateFactory2();
//        }
//        return uniqueInstance;
//    }

    //双重检查加锁,最优方案
    public static ChocolateFactory2 getInstance(){
        if (uniqueInstance == null) {
            synchronized (ChocolateFactory2.class){
                if (uniqueInstance == null) {
                    uniqueInstance= new ChocolateFactory2();
                }
            }
        }
        return uniqueInstance;
    }

    public void fill(){
        if(empty){
            //添加原料动作
            empty = false;
            boiled = false;
        }
    }

    public void drain(){
        if ((!empty) && boiled) {
            //排出巧克力
            empty = true;
        }
    }

    public void boil(){
        if ((!empty) && (!boiled)) {
            //加热
            boiled = true;
        }
    }
}
