package com.surpass.designer.No8_外观模式.facademode.hometheater;

/**
 * 最少知识原则说明
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 16:54
 */
public class Car {
//    Engine engine;//1.对象组件
//
//    public Car() {
//        //初始化发动机
//    }
//
//    //2.作为对象传进来的参数
//    public void start(Key mKey) {
//        Doors doors = new Doors(); //3.实例化的对象
//        boolean authorized = mKey.turns();//方法返回值获取的对象,违背原则
//        if (authorized) {
//            engine.start();
//            doors.lock();
//        }
//    }
}
