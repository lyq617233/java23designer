package com.surpass.designer.No8_外观模式.facademode;

import com.surpass.designer.No8_外观模式.facademode.hometheater.HomeTheaterFacade;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 16:41
 */
public class MainTest {
    public static void main(String[] args) {
        HomeTheaterFacade mHomeTheaterFacade = new HomeTheaterFacade();

        mHomeTheaterFacade.ready();
        mHomeTheaterFacade.play();
    }
}
