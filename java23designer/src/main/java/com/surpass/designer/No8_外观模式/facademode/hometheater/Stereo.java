package com.surpass.designer.No8_外观模式.facademode.hometheater;

/**
 * 音响
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/15 16:23
 */
public class Stereo {
    private static Stereo instance = null;
    private int volume = 5;

    private Stereo(){}

    public static Stereo getInstance() {
        if (instance == null) {
            instance = new Stereo();
        }
        return instance;
    }

    public void on(){
        System.out.println("Stereo On");
    }

    public void off(){
        System.out.println("Stereo Off");
    }

    public void setVolume(int vol) {
        volume = vol;
        System.out.println("the volume of Stereo is set to " + volume);
    }

    public void addVolume() {
        if (volume < 11) {
            volume++;
            setVolume(volume);
        }
    }

    public void subVolume() {
        if (volume > 0) {
            volume--;
            setVolume(volume);
        }
    }

}
