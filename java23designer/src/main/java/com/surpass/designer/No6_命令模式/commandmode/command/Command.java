package com.surpass.designer.No6_命令模式.commandmode.command;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:54
 */
public interface Command {
    public void execute();
    public void undo();
}
