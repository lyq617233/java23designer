package com.surpass.designer.No6_命令模式.commandmode.command;

/**
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:55
 */
public class NoCommand implements Command {

    @Override
    public void execute() {

    }

    @Override
    public void undo() {

    }
}
