package com.surpass.designer.No6_命令模式.commandmode.command;

import com.surpass.designer.No6_命令模式.commandmode.device.Stereo;

/**
 * 音响增加音量命令封装成对象
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 23:02
 */
public class StereoAddVolCommand implements Command{

    private Stereo stereo;

    public StereoAddVolCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        int vol = stereo.GetVol();
        if (vol < 11) {
            stereo.SetVol(++vol);
        }
    }

    @Override
    public void undo() {
        int vol = stereo.GetVol();
        if (vol > 0) {
            stereo.SetVol(--vol);
        }
    }
}
