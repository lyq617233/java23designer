package com.surpass.designer.No6_命令模式.commandmode.command;

import com.surpass.designer.No6_命令模式.commandmode.device.Stereo;

/**
 * 音响关闭命令封装成对象
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 23:00
 */
public class StereoOffCommand implements Command {

    private Stereo stereo;

    public StereoOffCommand(Stereo stereo) {
        this.stereo = stereo;
    }

    @Override
    public void execute() {
        stereo.Off();
    }

    @Override
    public void undo() {
        stereo.On();
        stereo.SetCd();
    }
}
