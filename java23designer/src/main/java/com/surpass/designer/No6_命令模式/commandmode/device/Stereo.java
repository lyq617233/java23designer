package com.surpass.designer.No6_命令模式.commandmode.device;

/**
 * 音响
 *
 * @author cmqzyd0700@163.com
 * @version 1.0
 * @since 2019/6/9 22:17
 */
public class Stereo {
    static int volume = 0;

    public void On(){
        System.out.println("Stereo On");
    }

    public void Off(){
        System.out.println("Stereo Off");
    }

    //选择CD光碟
    public void SetCd(){
        System.out.println("Stereo SetCd");
    }

    //设置音量
    public void SetVol(int vol){
        volume = vol;
        System.out.println("Stereo volume=" + volume);
    }

    public int GetVol(){
        return volume;
    }

    public void Start(){
        System.out.println("Stereo Start");
    }
}
